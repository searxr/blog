# Bring it DOM!

(Somebody please help us with better pun's) ;]<br><br>
Delivering HTML-like experiences in WebXR (without HTML) is quite a jump back in time.<br>
An hybrid approach, would be interesting to combine the best of both worlds.<br>
Let's see what's available:<br>

<img src="$(render url)/assets/media/bringitdom.JPG" style="width:100%;filter:hue-rotate(60deg)"/>

As you can see, as soon as things get smartphone/standalone stereoscopic, the CSS/DOM-possibilities dissappear.

> For stereoscopic experiences, SearXR emulates a (stripped-down version of) DOM in WebXR.

Just take a look at <a href="https://searxr.me">SEARXR</a>, and see how this hybrid mix can work on various devices:

<center><img src="$(render url)/assets/media/ARsearch.gif" style="width:50%;border:2px solid #CCC; border-radius:4px;"/></center>

The HTML search-textfield 'lives' inside 3D world (as an overlay), but is not rendered using the Webgl &lt;canvas&gt; element.<br>
This hybrid HTML/WebGL combination allows for the best of both worlds.<br>
In VR mode, the textfield is automatically projected as a canvas-texture, without losing its DOM-reference.

<center><img src="$(render url)/assets/media/ARdragging.gif" style="width:100%;border:2px solid #CCC; border-radius:4px;"/></center>

Spatial dragging of searchresults is another area which SearXR is researching.

> NOTE: Big respect to all <a href="https://en.wikipedia.org/wiki/VRML">VRML research</a>, which has directly and indirectly paved the way for many DOM-like tools & technology (also AFRAME, react-three-fiber ).
