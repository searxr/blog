# Pwa / store 

<br>
<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/installedpwa.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

> SearX is now available in .apk-format to broaden the exposure of the awesome SearX project.

## Download latest .apk manually

For android-based VR-devices:

* head over to [README.md](https://gitlab.com/searxr/aframe-searxr/-/tree/feat/immersive-result) 
* download and install the .apk-file.
* Profit!

## SideQuestVR 

SideQuest comes to closest to an 'F-droid of the Meta VR-devices'.<br>
As of today, SearXR is also submitted (currently in review) on SideQuest.<br>

> one step for man, one giant leap for SearXR!

<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/sidequest.webm" type="video/webm" />Your browser does not support the video element.</video></center>
