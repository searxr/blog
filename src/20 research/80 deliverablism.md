# The web does not care about deliverables

<br>
> NOTE: This post is about the Web (not WebXR for a change)

<br>
<img src="https://imgs.xkcd.com/comics/standards_2x.png" style="opacity:0.7;width:100%;"/>

Many people (including corporations) criticize the web as being too messy, therefore it supposedly needs moderation.<br>
I would like to counter this narrative with a the following question:<br>

> Why should messy become organised? If so, when would that 'organisation' be complete?

Yes the web is messy, it has a HUGE userbase, it has its heroes, villains, carnivores, vegans etc.<br>
In a sense, it's <b>a very diverse, open and infinite</b> ecosystem, comparable to <b>a forest</b>.<br>
If we organise this forest, would it still be a forest?<br>

> Also, could moderation break the whole ecology apart? What about messy-by-design?

Ignoring all this, might expose an interesting cognitive dissonance, which I call <b>'deliverableism'</b>: <br>
<br>

> "Expecting the organized from the messy"

Take these (false imho) narratives:

<div style="padding:20px; background:#F0F0F0;font-size: 17px;">
* "BigTech controls the web because somebody has to protect the people"
* "Power to the people, lets save the web from BigTech using IPFS and blockchain!"<br>
</div>

Both represent <b>deliverableism</b>, by sharing the following similarities:

<div style="padding:20px; background:#F0F0F0;font-size: 17px;">
* it inherits the <a href="https://en.wikipedia.org/wiki/XY_problem" target="_blank">XY problem</a>, by fixating on a certain <b>NEW</b> solution.<br>
* solutions are built using byproducts of the problem (*)<br>
* the moderation <b>HAS</b> to happen <b>by the</b> software (not the individual moderating its own behaviour)<br>
* in many cases the premise is vague & infinite, like "the internet is broken", "the web is unsafe", "companies cannot be trusted", "protect the children" e.g.<br>
* promoting inclusiveness but at the same time excluding important ecological aspects (banning bigtech, people moderating themselves e.g.)
</div>
<br>
> \* = For example: starting a Facebook Ad campaign against Facebook; Developers creating an alternative 'better web' accessible thru the 'broken web'. e.g.

When funds are allocated to the solution, the <a href="https://en.wikipedia.org/wiki/XY_problem" target="_blank">XY problem</a> gets buried in the solution.<br>
For example, halfway a <b>deliverablist</b>-project, it would be taboo to ask:

<div style="padding:20px; background:#F0F0F0;font-size: 17px;">
Q: "Why/when could our solution cause more harm to the initial problem?"<br>
Q: "When would it be better to turn this project into researchproject"<br>
Q: "Based on what we learned, how could our problem/solution be false?"
</div>

In <b>deliverableism</b> there's a taboo on adressing the <a href="https://en.wikipedia.org/wiki/XY_problem" target="_blank">XY problem</a>, once the project started.
Calling this <b>corporatism</b> would be a misstake, as governments and companies are free to define morphable deliverables.<br>

# Ideas: morphability

Deliverableism is not always bad (food-delivery), it just seems incompatible with complex problems, and enforces deliverable-is-success black/white-thinking.
Therefore, for solving complex problems:
 
<div style="padding:20px; background:#F0F0F0;font-size: 17px;">
* in project-proposals, promote morphable deliverables
* re-evaluate the <a href="https://en.wikipedia.org/wiki/XY_problem" target="_blank">XY problem</a> in every phase of a project<br>
* measure success based on morphability & retrospects <b>during</b> the problem<br>
* realize that the analysis-phase is usually based on imperfect understanding (which improves during the project)
</div>
