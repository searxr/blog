# WebXR Bookmarking 

<br>
Here's a rough preview of SearX(R) bookmarks:

<br>
<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/bookmarkannotation.webm" type="video/webm" />Your browser does not support the video element.</video></center>

SearX(R) is superexcited to announce: in the upcoming release XR bookmarking/url-sharing will be much easier!

> bookmarking/sharing urls has been a bit of a challenge in XR environments. SearX(R) is about to change this by bundling multiple bookmarks into one URL. 
