# Immersive searchresults 

<br>
<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/immersive.webm" type="video/webm" />Your browser does not support the video element.</video></center>

> In the upcoming release, spatial & immersive searchresults will be enabled. NOTE: the searchresults are random in this working demo.

## Bookmarking 

By pressing the plus-icon, searchresults can be imported into the scene.

## Visiting

By pressing the arrow-icon, a link can be opened.

> NOTE: when in VR, devices like Quest will exit immersive mode.

## Limiting vertices

The best UX in VR is having a stable framerate.<br>
SearXR now has support for limiting the vertices of imported 3d objects.

## Annotations

<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/annotation.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

