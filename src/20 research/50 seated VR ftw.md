# Seated VR FTW!

There's many popular pre-assumptions concerning consuming VR.<br>
One of them, is that you have to be running around in a room or a metaverse of some sort.<br>
However, seated (or lying) VR might be more important from an accessibility, and relaxation point of view.<br>
<br>

<img src="$(render url)/assets/media/bond.jpg" style="width:100%;"/>

As you can see, It's not really fun to be James Bond after a long day of work.<br>
More Immersiveness? no thank you.<br>
A comfi turning chair perhaps? 
<br>
<br>
<center><img src="$(render url)/assets/media/chair.png" style="width:70%;"/></center>

Seated VR <a href="https://www.lifewire.com/why-you-should-use-vr-lying-down-5203574" target="_blank">(or even lying)</a> solves the motion sickness issue of VR, as well as an accessibility issue of standing VR.</a><br>
Thats why at SearXR focuses on seated-VR-first approach.<br>
<br>

> In other words: perhaps we can consider the importance of fixed-camera seated immersiveness?

