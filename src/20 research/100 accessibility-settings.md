# Accessibility & Controller Settings 

<br>
Probably this will be extended in the future, but SearXR now has:

* a controller-section (see controller-button)
* it will open an accessibility screen with various toggles

<br>
<center><video width="100%" controls loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/offline-tts.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

> For example, offline text-to-speech or offline facetracker (using webcam) can be enabled 

# Text to Speech

SearXR uses an offline text-to-speech engine ♥ <br>
That means it's privacy-friendly, and no audio is sent to any server.<br> 
After having tried many offline engines, the [eSpeak](https://github.com/kripken/speak.js) based ones (like meSpeak) turned out
to be the most convenient.<br>
It's a nice, somewhat-robotic voice, which can be used for multiple (accessibility) future use-cases.

# Speech input

Unfortunately after lots of experiments & research, the WebSpeechApi implementation is on hold.<br>
Reasons:

* browsersupport of W3C WebSpeechAPI has stagnated
* the error-rate of 100% javascript speech-recognition-libraries is too high 

The above has been described in greater detail in [my report](https://gist.github.com/coderofsalvation/111b322900161128c8446dd514742c8d)

# Facetracking, Eyetracking, Handtracking

Below is a small demo of facial navigation using libraries like <a href="https://webgazer.cs.brown.edu" target="_blank">Webgazer.js</a> and <a href="https://www.npmjs.com/package/handsfree" target="_blank">handsfree.js</a>.<br>
It is not meant as a turn-key solution, but as a starting point for specific accessibility needs:

<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/facialtracking.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

> The webcam detects and tracks face-movement/rotations, which get reflected inside SearXR

This allows for interesting accessibility- or exhibition (project on wall) usecases.
