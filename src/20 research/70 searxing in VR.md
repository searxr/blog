# SearXing in XR

> The holy grail is to come up with some kind of universal search interface.<br>(Famous last words)

Initially, the current milestone was to project the <a href="https://github.com/searx/searx" target="_blank">SearX</a> desktop search-interfaces onto XR-devices somehow.<br>
This works quite well across desktop, mobile <b>and</b> tablet:

<center><video width="100%"" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/desktopmobilesearch.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

However, a lesson learned is that it doesn't translate well enough to VR..<br>

> In VR, a 'wearable' searchbar might be a better idea: a searchbar-on-wrist

<center><video width="100%"" autoplay loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/wristsearch.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

As you can see, a <a href="https://github.com/searx/searx" target="_blank">SearX</a> user in XR is always ready to search, by equipping the user with a keyboard (like an artist using a palette ).<br>
This is much more comfortable compared to a searchbar floating around your head..all the time.<br>
<br>
Stay tuned because we've got an speechrecognition-fueled-search planned too! 🎤<br>

> Obviously, it will be privacy-respecting too 💖💪
