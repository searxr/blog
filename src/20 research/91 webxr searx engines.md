# WebXR SearX engines 

<br>
The SearXR template for SearX has several WebXR engines, which facilitate discovery of WebXR destinations:

<br>
<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/searxconnectors.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

> Shown: the awesome opensource <a href="https://moonrider.xyz">moonrider WebXR experience</a> and a 3d model of the moon from a model-library website.
