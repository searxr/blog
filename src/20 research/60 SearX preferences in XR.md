# Managing SearX preferences in XR

The opensource [SearX](https://searx.me) engine has quite some bells and whistles, like autocomplete:<br>

<img src="$(render url)/assets/media/paris_google.png" style="max-width:700px"/>

> Did you know that **SearX** allows you to choose your auto-completion provider? ❤︎ 

That's right, you can let others (wikipedia e.g.)  complete you, with different suggestions.<br>
In the image above, you can see a mainstream searchengine autocomplete, and here is wikipedia's autocompletion:

<img src="$(render url)/assets/media/paris_wikipedia.png" style="max-width:700px"/>

> Wait..hold on..do you realize how powerful this is?

This is a nice opportunity for universities and companies, to implement their own autocomplete-engines (search thru papers e.g.).<br>
Just imagine for a moment, how you could organize your company's information in a totally different way.<br>
The potential is huge: it's filling the gap somewhere between webbookmarks and knowledgebases. 

# How to set SearX preferences in XR?

<img src="$(render url)/assets/media/oculus.png" style="max-width:700px"/>

> Luckily, VR headsets support the 2D web these days..so we can open a tab with the SearX preferences-page<br>

I did some counting, SearXR has more than 50 preference-settings and _even more_ searchengines to cherry-pick from.<br>
Amazing ❤︎ as that is, translating that to a VR does not make much sense.
Unless you like an interface like this:

<img src="$(render url)/assets/media/decision-paralysis.jpg"/>

# Progressive enhancement

SearXR <b>also offers</b> changing preferences <b>inside immersive mode</b>.<br>
In this mode tabs cannot be opened:

<img src="$(render url)/assets/media/preference-matrix.png"/>

Therefore SearX will utilitize preferences-URI's as a fallback:

<img src="$(render url)/assets/media/preference-uri.png" style="max-width:700px"/>

Here you can see preferences-URI's offered as buttons in <b>immersive mode</b> (as a fallback):

<img src="$(render url)/assets/media/preference-settings.gif" style="max-width:700px"/>

> For Universities, Companies and Non-profit organisations: you can configure sane defaults for your students/employees in <code>settings.yml</code> of your <a href="https://searx.github.io/searx/">SearX</a> installation
