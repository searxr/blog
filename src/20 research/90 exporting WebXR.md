# Exporting WebXR

<br>
SearXR can export a scene to either:

* a share-able URL
* a high-resolution 360 PNG-file
* a HTML-file 

<br>
<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/exports.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

> The parity between WebXR and exported HTML is not perfect, but we're definately getting there!<br>

Using these formats, SearXR searches can easily be shared in multiple WebXR ways:

* a snapshot of searchresults (360 image or HTML)
* a realtime URL (which performs the search in realtime)

> How to use the HTML export?

Simply by embedding.<br>
For example using an iframe.<br>
Positioning can be changed afterwards with few adjustments (perspective CSS-var).<br>
Right now the export is HTML/CSS-only for privacy reasons.<br>
Maybe in the future, optional wrappers like simple navigation and autoscaling could be added.

> How to load the 360 png?

360 images are widely supported by Web(XR) websites and VR headsets:

<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/360.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>

* online web 360 viewers (see example above), 
* photo gallery-apps of VR headsets
* javascript examples like <a href"https://threejs.org/examples/?q=pano">this THREE.js example</a> or <a src="https://aframe.io/examples/showcase/sky/">this AFRAME example</a>.

> How to share an URL?

Simple, just copy-paste the url!<br>
**NOTE**: Only the searchresults will be saved into the url.<br>
In-app navigation will not be recorded due to privacy reasons (cross-origin tracking).<br>

<center><video width="100%" autoplay muted loop style="border:2px solid #CCC; border-radius:4px;"><source src="$(render url)/assets/media/exporturl.mp4" type="video/mp4" />Your browser does not support the video element.</video></center>
